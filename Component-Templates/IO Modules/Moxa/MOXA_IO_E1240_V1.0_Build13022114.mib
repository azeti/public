-- *****************************************************************
-- Moxa Active Ethernet I/O Server MIB
--
-- 01-03-2013
--
-- Copyright (c) 2013 by Moxa Automation Co., Ltd.
-- All rights reserved.
-- *****************************************************************

MOXA-IO-E1240-MIB DEFINITIONS ::= BEGIN
    IMPORTS
        enterprises, Unsigned32, Integer32, MODULE-IDENTITY, OBJECT-TYPE FROM SNMPv2-SMI;
   
-- 1.3.6.1.4.1.8691.10.1240
    e1240   MODULE-IDENTITY
    LAST-UPDATED "201301031400Z"
    ORGANIZATION "Moxa Automation,Inc."
    CONTACT-INFO
            "Postal: Moxa Automation,Inc.
             Fl.4, No.135,Lane 235,Pao-Chiao Rd.
             Shing Tien City,Taipei,Taiwan,R.O.C
             Tel: +866-2-89191230 "
    DESCRIPTION
            "The MIB module for Moxa ioLogik Remote Ethernet I/O specific information." 
    REVISION "201301031400Z"
    DESCRIPTION
            "First version of this MIB."
           ::= { ioLogik 1240 }    -- 1.3.6.1.4.1.8691.10.1240

-- 1.3.6.1.4.1.8691
    moxa OBJECT IDENTIFIER ::= { enterprises 8691 }

-- 1.3.6.1.4.1.8691.10
    ioLogik OBJECT IDENTIFIER ::= { moxa 10 }

-- 1.3.6.1.4.1.8691.10.1240.1
    totalChannelNumber OBJECT-TYPE
        SYNTAX Integer32 (1..16)
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "Total I/O channels."
        ::= { e1240 1 }

-- 1.3.6.1.4.1.8691.10.1240.2
    serverModel OBJECT-TYPE
        SYNTAX OCTET STRING
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The I/O server model."
        ::= { e1240 2 }

-- 1.3.6.1.4.1.8691.10.1240.3
    systemTime OBJECT-TYPE
        SYNTAX Integer32
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "I/O server up time (in seconds)."
        ::= { e1240 3 }

-- 1.3.6.1.4.1.8691.10.1240.4
    firmwareVersion OBJECT-TYPE
        SYNTAX OCTET STRING
        MAX-ACCESS read-only
        STATUS current
        DESCRIPTION
            "The firmware version."
        ::= { e1240 4 }


----------------------------------------------------------------
-- I/O
-- 1.3.6.1.4.1.8691.10.1240.10          
    e1240monitor OBJECT IDENTIFIER ::= { e1240 10 }
----------------------------------------------------------------
-- ai
aiTable OBJECT-TYPE
		SYNTAX 			SEQUENCE OF AIEntry
		MAX-ACCESS 		not-accessible
		STATUS 			current
		DESCRIPTION		"The ai channel monitor table"
		::= { e1240monitor 4 }

aiEntry OBJECT-TYPE
		SYNTAX			AIEntry
		MAX-ACCESS 		not-accessible
		STATUS 			current
		DESCRIPTION		"The ai channel monitor item"
		INDEX { aiIndex }
		::= { aiTable 1 }

AIEntry ::=	SEQUENCE {
		aiIndex		Integer32,
		aiEnable	Integer32,
		aiMode		Integer32,
		aiValue		Integer32,
		aiMin		Integer32,
		aiMax		Integer32
}
-------------------------------------------
aiIndex OBJECT-TYPE
		SYNTAX 			Integer32(0..7)
		MAX-ACCESS 		read-only
		STATUS 			current
		DESCRIPTION		"The ai channel index."
		::= { aiEntry 1 }
aiEnable OBJECT-TYPE
		SYNTAX 			Integer32(0..1)
		MAX-ACCESS 		read-write
		STATUS 			current
		DESCRIPTION		"The ai channel Enable/Disable. Disable=0, Enable=1"
		::= { aiEntry 2 }

aiMode OBJECT-TYPE
		SYNTAX 			Integer32(0..4)
		MAX-ACCESS 		read-write
		STATUS 			current
		DESCRIPTION		"The ai channel Mode. 0-10V=0, 4-20mA=1, 0-20mA=2, 4-20mA Burnout=4"
		::= { aiEntry 3 }


aiValue OBJECT-TYPE
		SYNTAX 			Integer32(0..65535)
		MAX-ACCESS 		read-only
		STATUS 			current
		DESCRIPTION		"The ai channel value (RAW)."
		::= { aiEntry 4 }
		
aiMin OBJECT-TYPE
		SYNTAX 			Integer32(0..65535)
		MAX-ACCESS 		read-only
		STATUS 			current
		DESCRIPTION		"The ai channel minimum value (RAW)."
		::= { aiEntry 5 }
		
aiMax OBJECT-TYPE
		SYNTAX 			Integer32(0..65535)
		MAX-ACCESS 		read-only
		STATUS 			current
		DESCRIPTION		"The ai channel maximum value (RAW)."
		::= { aiEntry 6 }






----------------------------------------------------------------
-- AI TRAP Greater
-- 1.3.6.1.4.1.8691.10.1240.22
    aiTrap_Greater OBJECT IDENTIFIER ::= { e1240 22 }

----------------------------------------------------------------
aiTrapG0 TRAP-TYPE
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-00 Greater Trap."
	::= 1

aiTrapG1 TRAP-TYPE
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-01 Greater Trap."
	::= 2

aiTrapG2 TRAP-TYPE
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-02 Greater Trap."
	::= 3

aiTrapG3 TRAP-TYPE       
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-03 Greater Trap."
	::= 4

aiTrapG4 TRAP-TYPE
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-04 Greater Trap."
	::= 5

aiTrapG5 TRAP-TYPE
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-05 Greater Trap."
	::= 6

aiTrapG6 TRAP-TYPE
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-06 Greater Trap."
	::= 7

aiTrapG7 TRAP-TYPE
	ENTERPRISE 		aiTrap_Greater
	DESCRIPTION 		"The ai channel-07 Greater Trap."
	::= 8


----------------------------------------------------------------
-- AI TRAP Smaller
-- 1.3.6.1.4.1.8691.10.1240.23
    aiTrap_Smaller OBJECT IDENTIFIER ::= { e1240 23 }

----------------------------------------------------------------
aiTrapS0 TRAP-TYPE
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-00 Smaller Trap."
	::= 1

aiTrapS1 TRAP-TYPE
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-01 Smaller Trap."
	::= 2

aiTrapS2 TRAP-TYPE
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-02 Smaller Trap."
	::= 3

aiTrapS3 TRAP-TYPE       
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-03 Smaller Trap."
	::= 4

aiTrapS4 TRAP-TYPE
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-04 Smaller Trap."
	::= 5

aiTrapS5 TRAP-TYPE
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-05 Smaller Trap."
	::= 6

aiTrapS6 TRAP-TYPE
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-06 Smaller Trap."
	::= 7

aiTrapS7 TRAP-TYPE
	ENTERPRISE 		aiTrap_Smaller
	DESCRIPTION 		"The ai channel-07 Smaller Trap."
	::= 8
----------------------------------------------------------------
-- Message TRAP
-- 1.3.6.1.4.1.8691.10.1240.30
	messageTrap OBJECT IDENTIFIER ::= { e1240 30 }

----------------------------------------------------------------
activeMessageTrap TRAP-TYPE
	ENTERPRISE 		messageTrap
	DESCRIPTION 	"The SNMP trap with active message"
	::= 1

END


