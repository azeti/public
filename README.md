# Public azeti Repository #

Welcome, here you'll find useful resources to extend your azeti Installation. We'll feature external integrations (such as Grafana) here as well as our collection of Component Templates.

* [Online Documentation](http://docs.azeti.net)
* [Contact Us](mailto:consulting  (at)  azeti.net)

## Component Templates ##

Templates are XML based configuration files that hold on to the connection and ingestion parameters for machines, sensors and devices. We did quite some internal testing and evaluation of many sensors and devices which we are providing here as [templates to download](/Component-Templates). 

You can easily pick a template matching your environment, import it to the azeti Control Panel. This allows you to connect complex sensor & actuator combination within minutes, without the hassle of editing baud rates, stop bits and registers.
## [Follow this guide on how to import Templates.](https://azetinetworks.atlassian.net/wiki/display/KB/How+to+import+external+Sensor+Templates) ##

## [Grafana](www.grafana.org) API Endpoint ##

We are happy to offer a native Grafana API endpoint starting from azeti Stack Version 1.1. See our [integrations how-to](https://azetinetworks.atlassian.net/wiki/display/SSCLSM/Grafana+Integration) for configuration guidance.

### Download Grafana Dashboards ###

We have configured some example dashboards which you can download in the [Grafana section](Grafana/Dashboards).

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)